package DocGhiExcel;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BaseTest {
    public static int totalFailCase = 0;
    public static int totalPassCase = 0;
    public XSSFSheet sheet;
    public XSSFWorkbook wb;
    protected String sheetName;
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String message = "";

    //Hàm đọc file test case
    public TestCase[] docFileExcel (String diaChiFile, String tenSheet){
        try {
            FileInputStream fileInputStream = new FileInputStream(diaChiFile);
            wb = new XSSFWorkbook(fileInputStream);
            //DataFormatter dataFormatter = new DataFormatter();
            sheet = wb.getSheet(tenSheet);
            TestCase[] testcases = null;
            if (sheet != null) {
                testcases = new TestCase[sheet.getLastRowNum()];
                System.out.println("Số dòng của file là: " + sheet.getLastRowNum());
                String dk = null;
                for (int i = 0; i <= sheet.getLastRowNum(); i++) {
                    if(i!=sheet.getLastRowNum()) {
                        testcases[i] = new TestCase();
                    }
                    for (int j = 0; j < sheet.getRow(0).getLastCellNum(); j++) {
                        dk = sheet.getRow(0).getCell(j).toString();
                        if (i != 0) {// Nếu dòng khác header thì lưu

                            switch (dk) {
                                case "Refs":
                                    testcases[i-1].setRefs(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Testcase":
                                    testcases[i-1].setDescription(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "EndPoint":
                                    testcases[i-1].setEndPoint(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Method":
                                    testcases[i-1].setMethod(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Header":
                                    testcases[i-1].setHeader(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Token":
                                    testcases[i-1].setToken(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Body":
                                    testcases[i-1].setBody(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "StatusCode":
                                    testcases[i-1].setStatusCode(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "ExpectedResult":
                                    testcases[i-1].setExpectedResult(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Actual Response":
                                    testcases[i-1].setActualResponse(sheet.getRow(i).getCell(j).toString());
                                    break;
                                case "Result":
                                    testcases[i-1].setResult(sheet.getRow(i).getCell(j).toString());
                                    break;
                                default:
                                    break;
                            }

                        }
                    }

                }
                fileInputStream.close();
                //wb.close();
                return testcases;

            }else{
                fileInputStream.close();
                //wb.close();
                System.out.println("Không tìm thấy sheet");
                return null;
            }
        }catch (NullPointerException npointer) {
            npointer.printStackTrace();
            System.out.println("Cannot open the file because the current file path is null.");
        } catch (FileNotFoundException nfound){
            nfound.printStackTrace();
            System.out.println("Didn't find the file " + diaChiFile + ". Please check file path or file name.");
        } catch (IOException io){
            io.printStackTrace();
        }
        return null;
    }
    //Hàm đọc full file test case
    public TestCase[] docFullFileExcel (String diaChiFile, String tenSheet){
        try {
            FileInputStream fileInputStream = new FileInputStream(diaChiFile);
            wb = new XSSFWorkbook(fileInputStream);
            //DataFormatter dataFormatter = new DataFormatter();
            sheet = wb.getSheet(tenSheet);
            TestCase[] testcases = null;
            if (sheet != null) {
                testcases = new TestCase[sheet.getLastRowNum()+1];
                System.out.println("Số dòng của file là: " + (sheet.getLastRowNum()+1));
                String dk = null;
                for (int i = 0; i <= sheet.getLastRowNum(); i++) {
                    testcases[i] = new TestCase();
                    for (int j = 0; j < sheet.getRow(0).getLastCellNum(); j++) {
                        dk = sheet.getRow(0).getCell(j).toString();
                        // if (i != 0) {// Nếu dòng khác header thì lưu

                        switch (dk) {
                            case "Refs":
                                testcases[i].setRefs(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "Testcase":
                                testcases[i].setDescription(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "EndPoint":
                                testcases[i].setEndPoint(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "Method":
                                testcases[i].setMethod(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "Header":
                                testcases[i].setHeader(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "Token":
                                testcases[i].setToken(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "Body":
                                testcases[i].setBody(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "StatusCode":
                                testcases[i].setStatusCode(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "ExpectedResult":
                                testcases[i].setExpectedResult(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "Actual Response":
                                testcases[i].setActualResponse(sheet.getRow(i).getCell(j).toString());
                                break;
                            case "Result":
                                testcases[i].setResult(sheet.getRow(i).getCell(j).toString());
                                break;
                            default:
                                break;
                        }


                    }

                }
                fileInputStream.close();
                //wb.close();
                return testcases;

            }else{
                fileInputStream.close();
                //wb.close();
                System.out.println("Không tìm thấy sheet");
                return null;
            }
        }catch (NullPointerException npointer) {
            npointer.printStackTrace();
            System.out.println("Cannot open the file because the current file path is null.");
        } catch (FileNotFoundException nfound){
            nfound.printStackTrace();
            System.out.println("Didn't find the file " + diaChiFile + ". Please check file path or file name.");
        } catch (IOException io){
            io.printStackTrace();
        }
        return null;
    }

    //Hàm ghi file Excel report
    public void ghiFileExcel (TestCase[] testCase,String diaChiFile){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(diaChiFile);
            //Tạo một sheet mới
            for (int i = 0; i < testCase.length; i++) {
                //Tạo hàng thứ 0 sử dụng phương thức
                    XSSFRow row = sheet.createRow((short) i+1);
                    row.createCell(0, CellType.STRING).setCellValue(testCase[i].getRefs());
                    row.createCell(1, CellType.STRING).setCellValue(testCase[i].getDescription());
                    row.createCell(2, CellType.STRING).setCellValue(testCase[i].getEndPoint());
                    row.createCell(3, CellType.STRING).setCellValue(testCase[i].getMethod());
                    row.createCell(4, CellType.STRING).setCellValue(testCase[i].getHeader());
                    row.createCell(5, CellType.STRING).setCellValue(testCase[i].getToken());
                    row.createCell(6, CellType.STRING).setCellValue(testCase[i].getBody());
                    row.createCell(7, CellType.STRING).setCellValue(testCase[i].getStatusCode());
                    row.createCell(8, CellType.STRING).setCellValue(testCase[i].getExpectedResult());
                    row.createCell(9, CellType.STRING).setCellValue(testCase[i].getActualResponse());
                    row.createCell(10, CellType.STRING).setCellValue(testCase[i].getResult());

            }
            wb.write(fileOutputStream);
            fileOutputStream.close();
            wb.close();
            System.out.println("Đã ghi file thành công!");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Không tìm thấy file");

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Không tìm thấy địa chỉ");
        }


    }

    public boolean hamKiemTraKetQua(String response, TestCase testcase) {
        // Nếu status code khác rỗng, kiểm tra status code
        boolean result = true;
        int temp = 0;
        if(!testcase.getStatusCode().equals("")){
            message = "";
            Object objResponse = Configuration.defaultConfiguration().jsonProvider().parse(response);
            int i_check = 0;
            //Kiểm tra trường hợp kết quả trả ra có nhiều biến.
            if(testcase.getExpectedResult().contains(";")){
                String[] arrKeyVerify = testcase.getExpectedResult().split(";");
                for(int i = 0;i<arrKeyVerify.length; i++){
                    i_check = hamSoSanhChuoi(arrKeyVerify[i], objResponse);
                    if(i_check==0){
                        System.out.println("Kết quả này đúng rồi!");
                    }else{
                        temp = temp + i_check;
                        System.out.println("Kết quả này SAI rồi.........!");
                    }
                }

            }else{//Trường hợp trả ra có 1 biến
                temp = hamSoSanhChuoi(testcase.getExpectedResult(), objResponse);
            }


        } else {// Nếu status code là rỗng thì lấy nguyên response so sánh với expected result
            return testcase.getExpectedResult().equalsIgnoreCase(response);
        }
        if(temp>0){
            result = false;
        }
        return result;

    }
    public int hamSoSanhChuoi(String keyVerify, Object objJson){
        int icheck = 0;
        if(keyVerify.contains("==")){
            icheck = hamSoSanh("==",keyVerify,objJson);
        }else if (keyVerify.contains("!=")){
            icheck = hamSoSanh("!=",keyVerify,objJson);
        }else if (keyVerify.contains(">=")) {
            icheck = hamSoSanh(">=", keyVerify, objJson);
        }else if (keyVerify.contains("<=")) {
            icheck = hamSoSanh("<=", keyVerify, objJson);
        }else if (keyVerify.contains(">")) {
            icheck = hamSoSanh(">", keyVerify, objJson);
        }else if (keyVerify.contains("<<")) {
            icheck = hamSoSanh("<<", keyVerify, objJson);
        }else if (keyVerify.contains("<")) {
            icheck = hamSoSanh("<", keyVerify, objJson);
        }else {
            System.out.println("Không tìm thấy toán tử so sánh");
            icheck = 1;
        }
        return icheck;

    }
    //Hàm lấy giá trị từ json
    public String readValueByJsonPath(Object json, String key){
        try{
            return JsonPath.read(json,key).toString();
        }catch (PathNotFoundException e){
           // e.printStackTrace();
            System.out.println("Không tìm thấy param " + key);
            return null;
        }
    }
    public int hamSoSanh (String toanTu, String keyVerify, Object objJson){
        int icheck = 0;
        String[] arrExpectResult = keyVerify.split(toanTu);
        String key = arrExpectResult[0].trim();
        String expectValue = arrExpectResult[1];
        String actualValue = readValueByJsonPath(objJson,key);
        System.out.println("Key: "+key);
        String toanTuTam ="";
        if(!toanTu.equals("==")){
            toanTuTam = toanTu;
        }
        System.out.println("Expect: "+ toanTuTam + expectValue);
        System.out.println("Actual: "+ actualValue);
        switch (toanTu) {
            case "==": {
                if (expectValue.equals(actualValue))
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + " - Kết quả trả ra khác với giá trị mong muốn là: "+ toanTuTam  + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case "!=": {
                if (!expectValue.equals(actualValue))
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case ">=": {
                if (Integer.valueOf(actualValue) >= Integer.valueOf(expectValue) )
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case "<=": {
                if (Integer.valueOf(actualValue) <= Integer.valueOf(expectValue) )
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case ">": {
                if (Integer.valueOf(actualValue) > Integer.valueOf(expectValue) )
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case "<": {
                if (Integer.valueOf(actualValue) < Integer.valueOf(expectValue) )
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + " - Kết quả trả ra khác với giá trị mong muốn là: " + toanTuTam + expectValue;
                    System.out.println(message);
                }
            }
            break;
            case "<<": {
                if (expectValue.contains(actualValue))
                    icheck = 0;
                else {
                    icheck = 1;
                    message = key + " - Kết quả trả ra không tồn tại trong giá trị mong muốn: " + expectValue;
                    System.out.println(message);
                }
            }
            break;
            default:
                break;

        }
        return icheck;
    }
}
