package DocGhiExcel;

import java.util.ArrayList;

public class TestCase {
    public String Refs = " ";
    public  String Description = " ";
    public String EndPoint = " ";
    public  String Method = " ";
    public  String Header = " ";
    public  String Token = " ";
    public  String Body = " ";
    public  String StatusCode = " ";
    public  String ExpectedResult = " ";
    public  String ActualResponse= " ";
    public  String Result= " ";
    public String Respone = " ";

    public TestCase() {
    }



    public String getRefs() {
        return Refs;
    }

    public void setRefs(String refs) {
        Refs = refs;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getEndPoint() {
        return EndPoint;
    }

    public void setEndPoint(String endPoint) {
        EndPoint = endPoint;
    }

    public String getMethod() {
        return Method;
    }

    public void setMethod(String method) {
        Method = method;
    }

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getBody() {
        return Body;
    }

    public void setBody(String body) {
        Body = body;
    }

    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public String getExpectedResult() {
        return ExpectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        ExpectedResult = expectedResult;
    }

    public String getActualResponse() {
        return ActualResponse;
    }

    public void setActualResponse(String actualResponse) {
        ActualResponse = actualResponse;
    }

    public String getResult() {
        return Result;
    }

    public void setResult(String result) {
        Result = result;
    }

    public String getRespone() {
        return Respone;
    }

    public void setRespone(String respone) {
        Respone = respone;
    }

}
