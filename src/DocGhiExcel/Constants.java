package DocGhiExcel;

public class Constants {
    public static String INPUT_PATH = System.getProperty("user.dir") + "/input/";
    public static String OUTPUT_PATH = System.getProperty("user.dir") + "/src/DocGhiExcel/Output/";
    public static String IMPORT_PATH = System.getProperty("user.dir") + "/import/";
    public static String IMAGE_PATH = System.getProperty("user.dir") + "/src/DocGhiExcel/Input/";
    public static String LOG_PATH = System.getProperty("user.dir") + "/logs/log4j/";
}