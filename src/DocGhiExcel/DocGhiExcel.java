package DocGhiExcel;

import org.apache.poi.hpsf.Array;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class DocGhiExcel extends BaseTest{
    int statusColorTab = 0;
    @Test
    public void run(){
        String diaChiFile = "src/DocGhiExcel/Input/data_promotion_staging.xlsx";
        String diaChiFileOutput = "src/DocGhiExcel/Output/data_promotion_staging_output.xlsx";
        String tenFile = "data_promotion_staging.xlsx";
        String tenSheet = "ZMA_OG";
        String method = null;
        String response = null;
        boolean result = false;
        TestCase[] testcases = null;
        TestCase[] testCaseFull = null;
        testcases = docFileExcel(diaChiFile,tenSheet);
 //       testCaseFull = docFullFileExcel(diaChiFile,tenSheet);
        BaseTest bt = new BaseTest();
        if(testcases!=null){
            for(int i=0;i<testcases.length;i++){
                System.out.println("Test case thứ "+(i+1) +" là :" +testcases[i].getRefs());
                // Chạy API
                response= APIController.hamChayAPI(testcases[i]);
               // System.out.println("Kết quả trả về là: " + response);
                // Nếu response trả về khác rỗng, khác null, thì kiểm tra status code và
                // expected Result
                if (response != null && !response.equals("")) {
                    result = hamKiemTraKetQua(response, testcases[i]);
                    testcases[i].setActualResponse(response);
                  //  testCaseFull[i+1].setActualResponse(response);

                    if(result == false){
                        statusColorTab++;
                        totalFailCase++;
                        testcases[i].setResult("FALSE");
                   //     testCaseFull[i+1].setResult("FALSE");

                    }else{
                        totalPassCase++;
                        testcases[i].setResult("PASSED");
                   //     testCaseFull[i+1].setResult("PASSED");
                    }

                    System.out.println("Kết quả của test case là: "+ result);
                    System.out.println(bt.getMessage());
                }
            }
        }else{
            System.out.println("Không có dữ liệu trong file");
        }
        if(testcases!=null){
            ghiFileExcel(testcases,diaChiFileOutput);
        }
    }
}
