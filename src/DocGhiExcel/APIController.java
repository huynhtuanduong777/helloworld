package DocGhiExcel;

import org.apache.commons.lang.StringEscapeUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class APIController {
    public static HttpURLConnection connection = null;

    public static String sendApiRequestWithCustomHeader(TestCase testCase) {
        //Helper.sleep(1000);
      /*  MyLogger.info("************************************************");
        MyLogger.info("Start to request with authorization ...");
        MyLogger.info("Method: " + method);
        MyLogger.info("Source: " + source);
        MyLogger.info("Payload: " + payload);
        MyLogger.info("Token: " + authorizationCode);*/
        assert (testCase.getEndPoint() != "" && testCase.getEndPoint() != null) : "API link is empty";

        String response = "";
        URL url = null;
        BufferedReader bufReader = null;
        HttpURLConnection conn = null;
        try {
            url = new URL(testCase.getEndPoint());
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(testCase.getMethod());
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("ReCaptchaResponse", "smc@123456");

            if(testCase.getHeader().contains(";")) // Multi param header
            {
                String arrMultiParamHeader[] = testCase.getHeader().split(";");
                for(String item:arrMultiParamHeader){
                    List<String> lstHeader = getValueParamHeader(item);
                    if(lstHeader!=null) {
                        conn.setRequestProperty(lstHeader.get(0).trim(), lstHeader.get(1).trim());
                      //  MyLogger.info("Header added: " + lstHeader.get(0).trim() + ": " + lstHeader.get(1).trim());
                    }
                }
            }else { //One param header
                List<String> lstHeader = getValueParamHeader(testCase.getHeader());
                if(lstHeader!=null) {
                    conn.setRequestProperty(lstHeader.get(0).trim(), lstHeader.get(1).trim());
                   // MyLogger.info("Header added: " + lstHeader.get(0).trim() + ": " + lstHeader.get(1).trim());
                }
            }
            conn.setDoOutput(true);
            conn.setDoInput(true);
            if(!testCase.getMethod().equals("GET")){
                if(!testCase.getBody().contains("N/A")){
                    DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
                    outStream.write(testCase.getBody().getBytes());
                    outStream.flush();
                    outStream.close();
                }
            }
           // MyLogger.info("ResponseCode: " + conn.getResponseCode());

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
               // System.out.println("Trước khi chạy API");
                bufReader = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
               // System.out.println("Sau khi chạy API");
            } else {
               // System.out.println("Trước khi chạy API Lỗi");
                bufReader = new BufferedReader(new InputStreamReader(conn.getErrorStream(), "UTF-8"));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            //BaseObject.jsonActualResult=ioe.toString();
            System.out.println("---\nAPI Controller got an " + ioe.getClass().getName() + "\n---");
        } finally {
            try {
                response = bufReader.readLine();
                if (bufReader != null) {
                    bufReader.close();
                }

                if(true){
                    /*MyLogger.info("Get the response after run: " + response);
                    MyLogger.info("************************************************");*/
                    System.out.println("Đã chạy được API");
                }

                conn.disconnect();
            }catch (Exception e){
                //BaseObject.jsonActualResult=e.toString();
                System.out.println("Bị lỗi ở đây 2");
            }
        }
        if(!response.contains("\"")) {
            response = StringEscapeUtils.unescapeJava(response);
        }


        return response;
    }

    public static List<String> getValueParamHeader(String strHeader){
        if(!strHeader.equals("")){
            String arrParamHeader[] = strHeader.split(":");
            List<String> lstHeader = new ArrayList<>(Arrays.asList(arrParamHeader));
            return lstHeader;
        }else
            return null;
    }
    public static String hamChayAPI(TestCase testCase){

        BufferedReader reader = null;
        String line = null;
        StringBuffer responseContent = new StringBuffer();
        try {
            URL url = new URL(testCase.getEndPoint());
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(testCase.getMethod());
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept-Charset", "UTF-8");
            connection.setRequestProperty("ReCaptchaResponse", "smc@123456");

            //Gắn Header

            if(testCase.getHeader().contains(";")) // Multi param header
            {
                String arrMultiParamHeader[] = testCase.getHeader().split(";");
                for(String item:arrMultiParamHeader){
                    List<String> lstHeader = getValueParamHeader(item);
                    if(lstHeader!=null) {
                        connection.setRequestProperty(lstHeader.get(0).trim(), lstHeader.get(1).trim());
                        //  MyLogger.info("Header added: " + lstHeader.get(0).trim() + ": " + lstHeader.get(1).trim());
                    }
                }
            }else { //One param header
                List<String> lstHeader = getValueParamHeader(testCase.getHeader());
                if(lstHeader!=null) {
                    connection.setRequestProperty(lstHeader.get(0).trim(), lstHeader.get(1).trim());
                    // MyLogger.info("Header added: " + lstHeader.get(0).trim() + ": " + lstHeader.get(1).trim());
                }
            }

            //Nếu là phương thức khác GET
            connection.setDoOutput(true);
            connection.setDoInput(true);
            if(!testCase.getMethod().equals("GET")){
                if(!testCase.getBody().contains("N/A")){
                    //Kiểm tra có phải là case upfile hay không?
                    // Indicate a POST request
                    if(testCase.getBody().contains("upfile")){
                        //Chạy hàm upfile
                        System.out.println("Đây case upfile");
                        upFile(testCase);
                    }else{
                        DataOutputStream outStream = new DataOutputStream(connection.getOutputStream());
                        outStream.write(testCase.getBody().getBytes());
                        outStream.flush();
                        outStream.close();

                    }

                }
            }

            //System.out.println(connection.getResponseCode()); // Should be 200
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            int status = connection.getResponseCode();
            if(status > 299){ //Trường hợp status bị lỗi
                reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
                while ((line=reader.readLine())!=null){
                    responseContent.append(line);
                }
                reader.close();

            }else{ // Trường hợp thành công
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line=reader.readLine())!=null){
                    responseContent.append(line);
                }
                reader.close();

            }

           // System.out.println(responseContent.toString());
            return responseContent.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.out.println("Đã có lỗi URL");
            return "Đã có lỗi URL";
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Đã có lỗi mở kết nối");
            return "Đã có lỗi mở kết nối";
        }
    }
//Có 2 loại biến
    //Biến thường. int, float..
    //Biến khác được tạo bằng class
    public static void upFile(TestCase testCase) {
        //dataTest=7;
        //System.out.println("Giá trị i trong hàm upFile: "+ dataTest);
        String[] fileUpload = testCase.getBody().split(":");
        List<String> contents = new ArrayList<>();
        PrintWriter writer = null;
        String CRLF = "\r\n"; // Line separator required by multipart/form-data.
        contents.add("START");
        contents.add("Content-Disposition: form-data; name=\"Type\"");
        contents.add("CRLF");
        contents.add("102");
        contents.add("START");
        contents.add("Content-Disposition: form-data; name=\"name\"");
        contents.add("CRLF");
        contents.add(fileUpload[1].trim());
        contents.add("START");
        contents.add("Content-Disposition: form-data; name=\"file\"; filename=\"" + fileUpload[1].trim() + "\"");
        contents.add("Content-Type: " + "multipart/form-data");
        contents.add("CRLF");
        contents.add("DATA");
        contents.add("CRLF");
        contents.add("END");

        // Task attachments endpoint
        File theFile = new File(Constants.IMAGE_PATH + fileUpload[1].trim());
        System.out.println("Địa chỉ file là: " +theFile);
        // A unique boundary to use for the multipart/form-data
        String boundaryNumber = Long.toHexString(System.currentTimeMillis());
        String boundaryValue = "moxieboundary" + boundaryNumber;
        // Construct the body of the request
        connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=----" + boundaryValue);


        //OutputStream output = null;
        try {
            // Indicate a POST request
            connection.setDoOutput(true);
            DataOutputStream outStream = new DataOutputStream(connection.getOutputStream());
            writer = new PrintWriter(new OutputStreamWriter(outStream, "UTF-8"));

        for(int i=0; i<contents.size(); i++){
            switch (contents.get(i)){
                case "START":
                    writer.append("------" + boundaryValue).append(CRLF);
                    break;
                case "END":
                    writer.append("------" + boundaryValue + "--").append(CRLF).flush();
                    break;
                case "CRLF":
                    writer.append(CRLF).flush();
                    break;
                case "DATA":
                    Files.copy(theFile.toPath(), outStream);
                    break;
                default:
                    writer.append(contents.get(i)).append(CRLF);
                    break;
            }
        }
            outStream.write(testCase.getBody().getBytes());
            outStream.flush();
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        writer.close();


    }


}
