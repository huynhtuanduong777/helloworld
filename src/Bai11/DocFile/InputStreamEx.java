package Bai11.DocFile;

import java.io.*;
import java.nio.Buffer;
import java.nio.file.Files;

public class InputStreamEx {
    public static void main(String[] args) throws IOException {
        // Hình ảnh, âm thanh, video => Luồng byte
        FileInputStream fileInputStream = new FileInputStream("src/Bai11/input/input.txt");

       //Đọc file
        /* int i=0;
        while ((i = fileInputStream.read())!= -1){ //Khi đã đọc hết file
            System.out.print(i);
        }*/
        //Đọc file cách 2 - file txt
        /*FileInputStream fileInputStream2 = new FileInputStream("src/Bai11/input/input.txt");
        int j=0;
        while ((j = fileInputStream2.read())!= -1){ //Khi đã đọc hết file
            System.out.print((char)j);
        }*/

        //Đọc file cách 3 - sử dụng bộ nhớ đệm để đọc file
        /*BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        int i=0;
        while ((i = bufferedInputStream.read())!= -1){ //Khi đã đọc hết file
            System.out.print(i);
        }*/
        //User user = new User("Duong", 30, 4.5f);
        //ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);

        fileInputStream.close();

        // =======================================================================//
        // File text => Luồng ký tự
        FileReader fileReader = new FileReader("src/Bai11/input/input.txt");
        /*int i=0;
        while ((i = fileReader.read())!= -1){ //Khi đã đọc hết file
            System.out.print((char)i);
        }
        fileReader.close();*/
        BufferedReader bufferedReader = new BufferedReader(fileReader, 10); // 10 byte là đưa ra màn hình consose
        String line = null;
        while ((line = bufferedReader.readLine())!= null){
            System.out.println(line);
        }
        bufferedReader.close();
        fileReader.close();




        System.out.println("\nChạy thành công!");
    }
}
class User{
    private String name;
    private int age;
    private float gpa;

    public User(String name, int age, float gpa) {
        this.name = name;
        this.age = age;
        this.gpa = gpa;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public float getGpa() {
        return gpa;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }
}