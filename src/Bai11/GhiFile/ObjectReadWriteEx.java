package Bai11.GhiFile;

import java.io.*;

public class ObjectReadWriteEx {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        FileOutputStream  fileOutputStream = new FileOutputStream("src/Bai11/output/output.txt", true);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        User user = new User("Huỳnh Tuấn Dương", 15, 9.5f);
        User user2 = new User("Huỳnh Tuấn Dương", 16, 9.0f);
        objectOutputStream.writeObject(user);
        objectOutputStream.writeObject(user2);
        objectOutputStream.close();
        fileOutputStream.close();

        System.out.println("Đã ghi file thành công");
        FileInputStream fileInputStream = new FileInputStream("src/Bai11/output/output.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        User[] users = new User[6];
        for (int i = 0; i<6; i++){
           // if (objectInputStream.readObject()!= null) {
                users[i] = (User) objectInputStream.readObject();
                System.out.println("User: " + users[i].getName() + " - " + users[i].getAge() + " - " + users[i].getGpa());
           //   }else break;
        }
       // User user1 = (User) objectInputStream.readObject();

        objectInputStream.close();
        fileInputStream.close();
        System.out.println("Đã đọc file thành công");

    }
}
class User implements Serializable {// Phân rã dữ liệu để đọc ghi file
    private String name;
    private int age;
    private float gpa;

    public User(String name, int age, float gpa) {
        this.name = name;
        this.age = age;
        this.gpa = gpa;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public float getGpa() {
        return gpa;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }
}
