package Bai11.GhiFile;

import java.io.*;
import java.nio.Buffer;

public class OutputStreamEx {
    public static void main(String[] args) throws IOException {
        //  Hình ảnh, âm thanh, video => Luồng byte => dùng
        /*FileOutputStream fileOutputStream = new FileOutputStream("src/Bai11/output/output.txt");
        byte[] by = {65, 66, 65}; // ASCII
        for(byte b : by) {
            fileOutputStream.write(b);
        }*/
        /*FileOutputStream fileOutputStream = new FileOutputStream("src/Bai11/input/videoOutput.mp4");
        FileInputStream fileInputStream = new FileInputStream("src/Bai11/input/video.mp4");
        int i = 0;
        while ((i = fileInputStream.read())!= -1){
            fileOutputStream.write(i);
        }
        fileInputStream.close();
        fileOutputStream.close();
*/
       /* long time;
        time = System.currentTimeMillis();
        FileInputStream fileInputStream = new FileInputStream("src/Bai11/input/video.mp4");
        FileOutputStream fileOutputStream = new FileOutputStream("src/Bai11/output/videoOutput.mp4");
        BufferedOutputStream bufferedWriter = new BufferedOutputStream(fileOutputStream, 100); //Bộ nhớ đệm 100 byte => ghi lên file
        int i=0;
        while ((i = fileInputStream.read())!= -1) { //Khi đã đọc hết file
            bufferedWriter.write(i);

        }
        System.out.println("Thời gian chạy: " + (System.currentTimeMillis() - time));
        bufferedWriter.close();
        fileInputStream.close();
        fileOutputStream.close();*/



        // =======================================================================//
        // File text => Luồng ký tự
        FileWriter fileWriter = new FileWriter("src/Bai11/output/output.txt");
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter, 100);
        fileWriter.write("Huỳnh 1");
        fileWriter.write("\nHuỳnh 2");
        bufferedWriter.newLine();
        bufferedWriter.write("Trần văn A");

        bufferedWriter.close();

        fileWriter.close();
        System.out.println("\nChạy thành công rồi");

    }
}
