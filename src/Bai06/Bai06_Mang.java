package Bai06;

public class Bai06_Mang {
    public static void main(String[] args) {
        int[] mang = new int[10];
        int[] mang2 = new int[]{10,5,8,9};
        mang = mang2;
        for(int i = 0;i < mang.length;i++){
            System.out.println("Số thứ "+(i+1) +" là: "+ mang[i]);
        }
        for (int item : mang) //for each
        {
            System.out.println(item);
        }
        //Tham trị và tham chiếu
        // Tham trị thì chuyền biến và không làm thay đổi biến
        // Tham chiếu là chuyền mảng và làm thay đổi giá trị của mảng

        int n = 10;
        System.out.println("Trước khi tham trị biến n= " + n);
        thamTri(n);
        System.out.println("Sau khi tham trị biến n= " + n);

        System.out.println("Trước khi tham trị biến mang2[0]= " + mang2[0]);
        thamChieu(mang2);
        System.out.println("Sau khi tham trị biến mang2[0]= " + mang2[0]);

        //Mảng 2 chiều
        int[][] mang2Chieu = new int[2][3];
        int[][] mang2Chieu2 = new int[][]{{1,2,3},{4,5,6}};
        for (int i=0;i<mang2Chieu2.length;i++){
            for (int j =0; j<mang2Chieu2[i].length; j++){
                System.out.println("giá trị thứ i["+i + "]j["+j+"] là: "+mang2Chieu2[i][j] );
            }
        }

        

    }
    public static void thamTri(int n){
        n+= 5;

    }
    private static void thamChieu(int arr[]){
        arr[0]+= 5;

    }
}
