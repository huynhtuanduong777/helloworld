package Bai03;

import java.util.Scanner;

public class Bai03_KhaiBaoBien {


    //Viết tắt psvm = khai báo hàm main
    //Viết tắt sout = câu lệnh in chữ
    //Phím tắt Ctrl + Shift + ?    = comment đoạn code
    //Phím tắt Ctrl + Alt + L  = format lại đoạn code






    //Biến instance
    private int soThuTu = 1;
    //Biến static
    static int soSinhVien = 10;
    //Biến hằng
    static final int HANG_SO = 20;
    public static void main(String[] args) {

        double diemToan, diemVan, diemLy = 0;

        long hocPhi = 10_000_000;
        //Đóng học phí chưa? true là có; false là không.
        boolean dongHocPhi = false;
        char c = 'C';
        String tenSinhVien = "Huỳnh Tuấn Dương";

        System.out.println("Điểm của " + soSinhVien + " sinh viên: ");
        System.out.println("Giá trị biến hằng là: " + HANG_SO);
        System.out.println("Học phí là: " + hocPhi);

        // Gán giá trị cho biến
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số sinh viên: ");
        soSinhVien = scanner.nextInt();
        System.out.println("Số sinh viên mới nhập là: " + soSinhVien);
        System.out.println("Nhập tên sinh viên: ");
        scanner.nextLine();
        tenSinhVien = scanner.nextLine();
        System.out.println("Tên sinh viên mới nhập là: " + tenSinhVien);




    }

}

