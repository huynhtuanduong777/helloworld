package Bai09.Abstract;

public abstract class AbstractClassEx {
    protected  String name;
    protected int age;

    public AbstractClassEx(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public AbstractClassEx() {
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void print(){
        System.out.println("Đây là làm Abstract cha");
    }
    public abstract void printAbstract();
}
