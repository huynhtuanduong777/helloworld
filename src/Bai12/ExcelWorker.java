package Bai12;

import Bai12.Helper.Helper;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Handler;

public class ExcelWorker {
private static FileInputStream file = null;
private static XSSFWorkbook wb = null;

    public ExcelWorker() {
    }

   /* public static XSSFWorkbook getExcelWorkBook(String diaChiFile) { //File không có sheet

        if (wb != null) {
            return wb;
        } else {
            try {
                file = new FileInputStream(diaChiFile);
                wb = new XSSFWorkbook(file);
                file.close();
                return wb;

            } catch (NullPointerException npointter) {
                npointter.printStackTrace();
                System.out.println("Không thể mở file do địa chỉ file là null");
            } catch (FileNotFoundException nfound) {
                nfound.printStackTrace();
                System.out.println("Không thể mở file do không tìm được địa chỉ file");
            } catch (IOException io) {
                io.printStackTrace();
                System.out.println("Lỗi đọc ghi file");

            }

        }
        return null;
    }*/
    /*public static XSSFWorkbook getExcelWorkBook(String diaChiFile, String tenSheet) { //File có nhiều sheet

        try {
            if (wb != null) {
                for (int i = 0; i < wb.getNumberOfSheets(); i++) {
                    if (wb.getSheetAt(i).getSheetName().equals(tenSheet))
                        return wb;
                }
            }
            file = new FileInputStream(diaChiFile);
            wb = new XSSFWorkbook(file);
            file.close();
            return wb;

        } catch (NullPointerException npointter) {
            npointter.printStackTrace();
            System.out.println("Không thể mở file do địa chỉ file là null");
        } catch (FileNotFoundException nfound) {
            nfound.printStackTrace();
            System.out.println("Không thể mở file do không tìm được địa chỉ file");
        } catch (IOException io) {
            io.printStackTrace();
            System.out.println("Lỗi đọc ghi file");

        }
        return null;
    }*/
    public static String getDuLieuTrongFileExcel(int dong, String tenCot, String tenSheet){
        String data = null;
        int sttCot = ExcelWorker.getSttCotBangTen(tenCot,tenSheet);
        Object[][] objects = ExcelWorker.getDuLieu(wb, tenSheet);
        if(objects == null)
            return null;
        else{
            data = Helper.decodeUnicodeString(objects[dong][sttCot].toString());
            return data;
        }

    }

    private static Object[][] getDuLieu(XSSFWorkbook wb, String tenSheet) {
        Object[][] objects = null;
        XSSFSheet actionSheet = ExcelWorker.getSheet(wb,tenSheet);
        if (actionSheet==null){
            return null;
        }
        int tongSoCot = actionSheet.getRow(0).getLastCellNum();
        int tongSoDong = actionSheet.getLastRowNum() - actionSheet.getFirstRowNum();
        objects = new Object[tongSoDong][tongSoCot];
        for (int i=0; i< tongSoDong; i++){
            for(int j=0;j<tongSoCot; i++){
                objects[i][j]=actionSheet.getRow(i).getCell(j).toString();
            }
        }
        return objects;

    }

    private static int getSttCotBangTen(String tenCot, String tenSheet) {
        XSSFSheet sheet = ExcelWorker.getSheet(wb,tenSheet);
        XSSFRow row = sheet.getRow(0);
        int sttCot = -1;
        for(int i=0;i<row.getLastCellNum();i++){
            if(row.getCell(i).getStringCellValue().trim().equals(tenCot))
                sttCot=i;
        }
        return sttCot;

    }

    private static XSSFSheet getSheet(XSSFWorkbook wb, String tenSheet) {
        XSSFSheet sheetData = null;
        if(wb == null){
            return null;
        }
        try{
            sheetData = wb.getSheetAt(wb.getSheetIndex(tenSheet));
            return sheetData;
        }catch (IllegalArgumentException illegal){
            illegal.printStackTrace();
            System.out.println("Lỗi không tìm thấy sheet "+ tenSheet);
        }
        return null;
    }

}
