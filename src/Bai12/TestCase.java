
package Bai12;


public class TestCase {
    protected  String refs = null;
    protected  String dscription = null;
    protected String endPoint = null;
    protected String method = null;
    protected String header = null;
    protected  String token = null;
    protected  String body = null;
    protected  String statusCode = null;
    protected  String expectedResult = null;
    protected  String actualResponse = null;
    protected  String result = null;

    public TestCase() {
    }

    public String getRefs() {
        return refs;
    }

    public String getDscription() {
        return dscription;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public String getMethod() {
        return method;
    }

    public String getHeader() {
        return header;
    }

    public String getToken() {
        return token;
    }

    public String getBody() {
        return body;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public String getActualResponse() {
        return actualResponse;
    }

    public String getResult() {
        return result;
    }

    public void setRefs(String refs) {
        this.refs = refs;
    }

    public void setDscription(String dscription) {
        this.dscription = dscription;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    public void setCctualResponse(String actualResponse) {
        this.actualResponse = actualResponse;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public TestCase(String refs, String dscription, String endPoint, String method, String header, String token, String body, String statusCode, String expectedResult, String cctualResponse, String result) {
        this.refs = refs;
        this.dscription = dscription;
        this.endPoint = endPoint;
        this.method = method;
        this.header = header;
        this.token = token;
        this.body = body;
        this.statusCode = statusCode;
        this.expectedResult = expectedResult;
        this.actualResponse = actualResponse;
        this.result = result;
    }
    public void printTestCase(){
        System.out.println("refs là: "+ refs);
        System.out.println("dscription là: "+ dscription);
        System.out.println("endPoint là: "+ endPoint);
        System.out.println("method là: "+ method);
        System.out.println("header là: "+ header);
        System.out.println("token là: "+ token);
        System.out.println("body là: "+ body);
        System.out.println("statusCode là: "+ statusCode);
        System.out.println("expectedResult là: "+ expectedResult);
        System.out.println("actualResponse là: "+ actualResponse);
        System.out.println("result là: "+ result);


    }
}


