package Bai07;

import java.util.Scanner;

public class Bai07_TinhDongGoi {
    public static void main(String[] args) {
        String[] name = {"Huỳnh","Huỳnh Tuấn","Huỳnh Tuấn Dương"};
        float[] dtb = {6.4f,4,7.9f};
        StudentOOP[] sinhViens = new StudentOOP[3];

        for(int i =0; i<3;i++){
            sinhViens[i] = new StudentOOP();
            sinhViens[i].name = name[i];
            sinhViens[i].dtb = dtb[i];
        }


        for(int i=0;i<3;i++){
            System.out.println("Sinh viên thứ "+ (i+1)+": " + sinhViens[i].name+ " - "+  sinhViens[i].dtb + " ;");
        }

        StudentOOP tempSV = new StudentOOP();
        for (int i=0; i<2; i++){
            for (int j=i+1; j<3; j++){
                if(sinhViens[i].dtb < sinhViens[j].dtb){
                    tempSV = sinhViens[i];
                    sinhViens[i] = sinhViens[j];
                    sinhViens[j] = tempSV;
                }
            }
        }
        System.out.println("Sau khi sắp xếp, danh sách sinh viên là: ");
        for(int i=0;i<3;i++){
            System.out.println("Sinh viên thứ "+ (i+1)+": " + sinhViens[i].name+ " - "+  sinhViens[i].dtb + " ;");
        }


        for (int i = 0; i < 3; i++) {
            if (sinhViens[i].checkDTB() == 1) {
                System.out.println("Sinh viên thứ " + (i + 1) + ": " + sinhViens[i].name + " - " + sinhViens[i].dtb + " là Sinh viên giỏi;");
            } else if (sinhViens[i].checkDTB() == 2) {
                System.out.println("Sinh viên thứ " + (i + 1) + ": " + sinhViens[i].name + " - " + sinhViens[i].dtb + " là Sinh viên Khá;");
            } else if (sinhViens[i].checkDTB() == 3) {
                System.out.println("Sinh viên thứ " + (i + 1) + ": " + sinhViens[i].name + " - " + sinhViens[i].dtb + " là Sinh viên Trung Bình;");
            } else
                System.out.println("Sinh viên thứ " + (i + 1) + ": " + sinhViens[i].name + " - " + sinhViens[i].dtb + " là Sinh viên Yếu;");
        }

        // Khai báo sinh viên mới bằng Constructer
        StudentOOP sinhVien2 = new StudentOOP("Duong", 6.7f, "Kiên Giang");
        // Khai báo sinh viên mới bằng Constructer
        sinhVien2.setGioiTinh("Nam");
        System.out.println("Sinh viên 2 là: " + sinhVien2.name + " - "+ sinhVien2.getGioiTinh());





    }
}
