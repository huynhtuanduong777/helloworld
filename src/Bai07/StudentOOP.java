package Bai07;

public class StudentOOP {

    //Public
    public String name;
    public float dtb;
    public String address;
    //Private
    private String gioiTinh;



    int checkDTB() {
        if (dtb >= 8) {// Sinh viên giỏi
            return 1;
        } else if (dtb < 8 && dtb >= 6.5) { // Sinh viên Khá
            return 2;
        } else if (dtb < 6.5 && dtb >= 5) { // Sinh viên trung bình
            return 3;
        } else
            return 4; // Sinh viên yếu
    }

    // Nhấn Alt + Insert để tạo constructer tự động

    public StudentOOP(String name, float dtb, String address) {
        this.name = name;
        this.dtb = dtb;
        this.address = address;
    }

    public StudentOOP() {
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }
}
