package Bai08;

import Bai08.Animals.Animals;
import Bai08.Animals.Chicken;
import Bai08.Animals.Dog;
import Bai08.Bank.BIDV;
import Bai08.Bank.BIDVChild;
import Bai08.Bank.VCB;

public class Bai08_TinhKeThua {

    public static void main(String[] args) {
        BIDV bi = new BIDV();
        bi.setName("Ngân hàng BIDV");
        bi.setLogo("AAA");
        bi.setLaiSuat(6.0f);
        VCB vcb = new VCB();
        vcb.setName("Ngân hàng Vietcombank");
        vcb.setLogo("BBB");
        vcb.setLaiSuat(6.5f);

        BIDVChild biChild = new BIDVChild();
        biChild.print();

        Animals dog, chicken;
        dog = new Dog();
        chicken = new Chicken();
        System.out.println("Số chân của chó: "+ dog.getLegs());
        System.out.println("Số chân của gà: "+ chicken.getLegs());


    }
}
