package Bai08.Bank;

public class Bank {
    private String name;
    private String logo;
    private  float laiSuat;

    public Bank() {
    }

    public Bank(String name, String logo, float laiSuat) {
        this.name = name;
        this.logo = logo;
        this.laiSuat = laiSuat;
    }

    public String getName() {
        return name;
    }

    public String getLogo() {
        return logo;
    }

    public float getLaiSuat() {
        return laiSuat;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setLaiSuat(float laiSuat) {
        this.laiSuat = laiSuat;
    }
}
