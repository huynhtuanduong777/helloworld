package Bai10;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Scanner;

public class Bai10_ExceptionEx {
    public static void main(String[] args) {
       /* int a = 10;
        int b = 0;
        int[] mang = new int[5];
        int age;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập số tuổi: ");
        scanner.nextInt();
        mang[5] = 1;
        System.out.println(a / b);
        System.out.println("Kết thúc");*/



        //Checked exception
      /*  try {
            byte[] file = Files.readAllBytes(new File("./test.txt").toPath());
        } catch (IOException e) {
            System.out.println("lỗi ở đây nè: "+ e);

        }
        System.out.println("Cuối cùng");*/

        // Uncheck exception
        /*try {
            int a = 10;
            int b = 0;
            System.out.println( a / b);
            System.out.println("Kết thúc");
        } catch (ArithmeticException e){
            System.out.println(" Lỗi: "+ e.getMessage());
        } catch (Exception f){
            System.out.println(" Lỗi 2 "+ f.getMessage());
        }*/
        System.out.println("Trước khi chia");
        System.out.println("Kết quả phép chia 5/0 là: "+ phepChia(5,0));
        System.out.println("Sau khi chia");


    }

    private static int phepChia (int a, int b){
        try{
            System.out.println(a/b);
            return a/b;
        }catch (NumberFormatException e){
            System.out.println("Lỗi number format "+ e);
            return -1;
        }catch (ArithmeticException e){
            System.out.println("Lỗi toán tử "+ e);
            return -2;
        }

    }
}
